<?php
    $warna = "merah";

    switch ($warna) {
        case 'pink':
            echo '<p style="color:pink">Ini Warna Pink</p>';
            break;
        case 'merah':
            echo '<p style="color:red">Ini Warna Merah</p>';
            break;
        case 'kuning':
            echo '<p style="color:yellow">Ini Warna Kuning</p>';
            break;
        case 'hijau':
            echo '<p style="color:green">Ini Warna Green</p>';
            break;
        default:
            echo '<p style="color:black">Ini Warna Default</p>';
            break;
    }
?>