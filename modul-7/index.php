<?php
    $name = "Alya Maulida";
    $nim = "6702210031";
    $email = "alyaamaulidaa@gmail.com";
    $noHP = "083183229318";
    $noWA = "6283183229318"; //Wajib pakai kode negara (kode Indonesia = 62)
    $tglLahir = "2003-05-14";
    $umur = hitungUmur($tglLahir);
    $background = '#001d3d';
    $color = 'rgb(211, 159, 63)';
    
    function hitungUmur($birthdayDate){
        $date = new DateTime($birthdayDate);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    echo "Nama Saya: ";
    echo $name;
    echo "<br>";
    echo "NIM Saya: ";
    echo $nim;
    echo "<br>";
    echo "Email Saya: ";
    echo " <a href='mailto://".$email."' target='_blank'>".$email."</a>";
    echo "<br>";
    echo "No HP Saya: ";
    echo " <a href='tel://".$noHP."' target='_blank'>".$noHP."</a>";
    echo "<br>";
    echo "No WA Saya: ";
    echo " <a href='https://wa.me/".$noWA."' target='_blank'>".$noWA."</a>";
    echo "<br>";
    echo "Tgl Lahir Saya: ";
    echo $tglLahir;
    echo "<br>";
    echo "Umur Saya: ";
    echo $umur." Tahun";
    echo "<br>";
    echo "2 Tahun yang lalu saya berumur ".($umur-2)." Tahun<br>";
    echo "Tahun depan saya akan berumur ".($umur+1)." Tahun<br>";
    echo "<a href='perhitungan.php'>Klik untuk belajar perhitungan dengan PHP</a>";
    echo "<br>";
    echo "<a href='lingkaran.php'>Klik untuk belajar menghitung luas lingkaran dengan PHP</a>";
    echo "<br>";
    echo "<a href='percabangan.php'>Klik untuk belajar percabangan dengan PHP</a>";
    echo "<br>";
    echo "<a href='perulangan.php'>Klik untuk belajar perulangan dengan PHP</a>";
    echo "<br>";
   
?>

<style>

    body{
        background: <?php echo $background; ?>;
        color: <?php echo $color; ?>;
    }
    a:link {
    color: #a2d2ff ;
  }
  a:visited {
    color: #a2d2ff;
  }
  a:hover {
    color: rgb(211, 159, 63) ;
  }
  
</style>







