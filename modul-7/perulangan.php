<?php
$background = '#001d3d';
$color = 'rgb(211, 159, 63)';

//Perulangan For
echo "<h2>Perulangan For</h2>";
for($i = 1; $i <= 10; $i++){
    echo "<h3>Angka ke-$i</h3>";
}

//Perulangan do while
echo "<h2>Perulangan Do While</h2>";
$ulangi = 10;
do {
    echo "<h3>Perulangan ke-$ulangi</h3>";
    $ulangi --;
} while ($ulangi > 0);

?>

<style>

    body{
        background: <?php echo $background; ?>;
        color: <?php echo $color; ?>;
    }
</style>